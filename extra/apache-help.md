Link to article: [https://holarails.wordpress.com/2014/04/28/how-to-change-the-domain-name-of-a-virtual-host-in-apache/](apache article)

How to change the domain name of a virtual host in Apache

This instructions will be useful for you if you have a webserver with Ubuntu 12.04 (I have not tested this solution with other versions) running Apache 2.2.

Starting point:

You have a domain configured as a virtualhost. For example: example1.com

Ending point:

You want to change it to example2.com

These are the steps you have to follow:

1. Modify ``/etc/hosts``

The line
```
127.0.0.1 example1.com
should be changed to
127.0.0.1 example2.com
```
2. Rename the file /etc/apache2/sites-available/example1.com to example2.com

``mv /etc/apache2/sites-available/example1.com /etc/apache2/sites-available/example2.com``

3. Rename the folder where you have the public content, that is the DocumentRoot Apache configuration.
E.g. in my case, this is ``/var/www/example1.com``
``mv /var/www/example1.com /var/www/example2.com``

4. Disable the former site and enable the new one:

``a2dissite example1.com``
``a2ensite example2.com``

On one hand, this will remove the symlink in /etc/apache2/sites-enabled that refers to /etc/apache2/sites-available.
And on the other this will create a new symlink for the new site.
Further details about a2ensite and a2disite here: http://man.he.net/man8/a2dissite

5. When you are done, restart Apache

``service apache2 restart``
