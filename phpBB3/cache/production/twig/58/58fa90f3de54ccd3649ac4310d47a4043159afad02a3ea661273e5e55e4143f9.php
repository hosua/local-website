<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @aurelienazerty_darkmode/event/overall_header_navigation_prepend.html */
class __TwigTemplate_b5f677ddda2276146f7c7684999ebfd2af1beb6cef02b2a6a0f01381735a2e04 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<li data-last-responsive=\"true\" class=\"rightside\" style=\"";
        echo ($context["STYLE_DO_LIGHT"] ?? null);
        echo "\" id=\"callLight\">
\t<a href=\"javascript:void(0);\" onclick=\"darkmode(false)\">
\t\t<i class=\"icon fa-lightbulb-o fa-fw\" aria-hidden=\"true\"></i><span>";
        // line 3
        echo ($context["DO_LIGHT_MESSAGE"] ?? null);
        echo "</span>
\t</a>
</li>
<li data-last-responsive=\"true\" class=\"rightside\" style=\"";
        // line 6
        echo ($context["STYLE_DO_DARK"] ?? null);
        echo "\" id=\"callDark\">
\t<a href=\"javascript:void(0);\" onclick=\"darkmode(true)\">
\t\t<i class=\"icon fa-moon-o fa-fw\" aria-hidden=\"true\"></i><span>";
        // line 8
        echo ($context["DO_DARK_MESSAGE"] ?? null);
        echo "</span>
\t</a>
</li>";
    }

    public function getTemplateName()
    {
        return "@aurelienazerty_darkmode/event/overall_header_navigation_prepend.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 8,  49 => 6,  43 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@aurelienazerty_darkmode/event/overall_header_navigation_prepend.html", "");
    }
}
